<?php
/**
 * Coolcsn Zend Framework 2 Authorization Module
 * 
 * @link https://github.com/coolcsn/CsnAuthorization for the canonical source repository
 * @copyright Copyright (c) 2005-2013 LightSoft 2005 Ltd. Bulgaria
 * @license https://github.com/coolcsn/CsnAuthorization/blob/master/LICENSE BSDLicense
 * @author Stoyan Revov <st.revov@gmail.com>
*/

namespace CsnAuthorization\Acl;

use Zend\Permissions\Acl\Acl as ZendAcl,
    Zend\Permissions\Acl\Role\GenericRole as Role,
    Zend\Permissions\Acl\Resource\GenericResource as Resource;

/**
 * Class to handle Acl
 *
 * This class is for loading ACL defined in a database
 *
 * @copyright Copyright (c) 2005-2013 LightSoft 2005 Ltd. Bulgaria
 * @license https://github.com/coolcsn/CsnAuthorization/blob/master/LICENSE BSDLicense
 */
class AclDb extends ZendAcl {
    /**
     * Default Role
     */
    const DEFAULT_ROLE = 'guest';

    /**
     * Constructor
     *
     * @param $entityManager Inject Doctrine's entity manager to load ACL from Database
     * @return void
     */
    public function __construct($entityManager)
    {
        $roles = $entityManager->getRepository('CsnUser\Entity\Role')->findAll();
        $resources = $entityManager->getRepository('CsnAuthorization\Entity\Resource')->findAll();
        $privileges = $entityManager->getRepository('CsnAuthorization\Entity\Privilege')->findAll();
        
        $this->_addRoles($roles)
             ->_addAclRules($resources, $privileges);
        $this->allow("guest", "CsnUser\Controller\Index", "login");

    }

    /**
     * Adds Roles to ACL
     *
     * @param array $roles
     * @return CsnAuthorization\Acl\AclDb
     */
    protected function _addRoles($roles)
    {
        foreach($roles as $role) {
            if (!$this->hasRole($role->getName())) {
                $parents = $role->getParents()->toArray();
                $parentNames = array();
                foreach($parents as $parent) {
                    $parentNames[] = $parent->getName();
                }
                $this->addRole(new Role($role->getName()), $parentNames);
            }
        }

        return $this;
    }

    /**
     * Adds Resources/privileges to ACL
     *
     * @param $resources
     * @param $privileges
     * @return User\Acl
     * @throws \Exception
     */
    protected function _addAclRules($resources, $privileges)
    {

        foreach ($resources as $resource) {
            if (!$this->hasResource($resource->getName())) {
                $this->addResource(new Resource($resource->getName()));
            }
        }
        $this->_addPrivilege($privileges);

        return $this;
    }
    protected function _addPrivilege($privileges,$privilege_id="",$role_name = "")
    {
        foreach ($privileges as $privilege) {

            if (is_object($privilege->getParents())) {
// error_log("teste privilege = $privilege_id privilege_id  = ".$privilege->getId()." label = ".$privilege->getLabel()." role.id = ".$privilege->getRole()->getId()." role.name = ".$privilege->getRole()->getName()." role_name = $role_name");
                if ($privilege_id>0)
                {
                    $this->_addPrivilege($privilege->getParents(),$privilege->getId(), $role_name);
                }else
                {
                    $this->_addPrivilege($privilege->getParents(),$privilege->getId(), $privilege->getRole()->getName());
                    $role_name="";
                }
            }
            if ($role_name == "") $role_name = $privilege->getRole()->getName();
            if ($privilege->getPermissionAllow()) {
                $this->allow($role_name, $privilege->getResource()->getName(), $privilege->getName());
// error_log($role_name.", ".$privilege->getResource()->getName().", ".$privilege->getName());
// error_log("Allow privilege_parent = $privilege_id privilege_id  = ".$privilege->getId()." label = ".$privilege->getLabel()." role.id = ".$privilege->getRole()->getId()." role = ".$role_name." resource = ".$privilege->getResource()->getName()." privilege = ".$privilege->getName());
            } else {
// error_log("Deny label = ".$privilege->getResource()->getLabel()." role.name = ".$role_name." resource = ".$privilege->getResource()->getName()." privilege = ".$privilege->getName());
                $this->deny($role_name, $privilege->getResource()->getName(), $privilege->getName());
            }
        }
        return true;
    }
}
