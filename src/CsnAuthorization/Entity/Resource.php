<?php
/**
 * Coolcsn Zend Framework 2 Authorization Module
 * 
 * @link https://github.com/coolcsn/CsnAuthorization for the canonical source repository
 * @copyright Copyright (c) 2005-2013 LightSoft 2005 Ltd. Bulgaria
 * @license https://github.com/coolcsn/CsnAuthorization/blob/master/LICENSE BSDLicense
 * @author Stoyan Cheresharov <stoyan@coolcsn.com>
 * @author Stoyan Revov <st.revov@gmail.com>
*/

namespace CsnAuthorization\Entity;

use Doctrine\ORM\Mapping as ORM;

use Zend\Form\Annotation; // !!!! Absolutely neccessary
use Zend\Stdlib\Hydrator;

/**
 * Resources
 *
 * @ORM\Table(name="resource")
 * @ORM\Entity(repositoryClass="CsnAuthorization\Entity\Repository\ResourceRepository")
 * @Annotation\Name("resource")
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Resource
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"min":1, "max":30}})
     * @Annotation\Validator({"name":"Regex", "options":{"pattern":"/^[a-zA-Z][a-zA-Z0-9_ -]{0,100}$/"}})
     * @Annotation\Attributes({"type":"text"})
     * @Annotation\Options({"label":"Resource:"})
     */
    protected $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Annotation\Exclude()
     * @ORM\OneToMany(targetEntity="privilege", mappedBy="resource")
     */
    protected $id;
    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=100, nullable=true)
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"encoding":"UTF-8", "max":100}})
     */
    protected $label;
    
    /**
     * @var CsnAuthorization\Entity\Resource
     *
     * @ORM\ManyToOne(targetEntity="CsnAuthorization\Entity\Resource")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id", nullable=true)
     * @Annotation\Type("DoctrineModule\Form\Element\ObjectSelect")
     * @Annotation\Options({
     * "label":"Resource:",
     * "empty_option": "Please, choose a resource",
     * "target_class":"CsnAuthorization\Entity\Resource",
     * "property": "name"})
     */
    protected $resource;

    /**
     * Set name
     *
     * @param  string   $name
     * @return Resource
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set label
     *
     * @param  string   $label
     * @return Resource
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    /**
     * Set resource
     *
     * @param  CsnAuthorization\Entity\Resource $resource
     * @return CsnAuthorization\Entity\Resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return CsnAuthorization\Entity\Resource
     */
    public function getResource()
    {
        return $this->resource;
    }
    public function __construct($options = array())
    {
        /*
        $hydrator = new Hydrator\ClassMethods;
        $hydrator->hydrate($options, $this);
        */
        (new Hydrator\ClassMethods)->hydrate($options,$this);
    }

    public function toArray()
    {
        return (new Hydrator\ClassMethods())->extract($this);
    }
}
