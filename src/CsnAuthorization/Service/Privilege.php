<?php

namespace CsnAuthorization\Service;

use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;
use Application\Service\AbstractService;

class Privilege extends AbstractService
{
	protected $view;
	public function __construct (EntityManager $em, $view) {
		parent::__construct($em);
		$this->entity = "CsnAuthorization\Entity\Privilege";
		$this->view = $view;
	}

	public function insert (array $data) {

		if (count($data["parents"])>0)
		{
			$itemsPrivilege = array();
			foreach ($data["parents"] as $key => $items) 
			{
				$itemsPrivilege[]=$this->em->find('CsnAuthorization\Entity\Privilege', $items["id"]);
			}
			$data["parents"] = $itemsPrivilege;
		}else{
			$data["parents"] = array();
		}
		$data["role"] = $this->em->getReference("CsnUser\Entity\Role", $data["role"]["id"]);
		$data["resource"] = $this->em->getReference("CsnAuthorization\Entity\Resource", $data["resource"]["id"]);
        return parent::insert($data);
	}

	public function update (array $data) {
		if (count($data["parents"])>0)
		{
			$itemsPrivilege = array();
			foreach ($data["parents"] as $key => $items) 
			{
				$itemsPrivilege[]=$this->em->find('CsnAuthorization\Entity\Privilege', $items["id"]);
			}
			$data["parents"] = $itemsPrivilege;
		}else{
			$data["parents"] = array();
		}
	   	$data["role"]=$this->em->find('CsnUser\Entity\Role', $data["role"]["id"]);
		$data["resource"] = $this->em->getReference("CsnAuthorization\Entity\Resource", $data["resource"]["id"]);
//    	unset($data["display_name"]);
      return parent::update($data);

	}
}