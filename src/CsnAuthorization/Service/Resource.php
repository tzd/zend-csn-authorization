<?php

namespace CsnAuthorization\Service;

use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;
use Application\Service\AbstractService;

class Resource extends AbstractService
{
	protected $view;
	public function __construct (EntityManager $em, $view) {
		parent::__construct($em);
		$this->entity = "CsnAuthorization\Entity\Resource";
		$this->view = $view;
	}

	public function insert (array $data) {
		error_log(serialize($data));
//			$data["role"] = $this->em->getReference("CsnUser\Entity\Role", 1);
		if ($data["resource"]["id"]>0) {
			$data["resource"] = $this->em->getReference("CsnAuthorization\Entity\Resource", $data["resource"]["id"]);
		}else{
			unset($data["resource"]);
		}
        return parent::insert($data);
	}

	public function update (array $data) {
   	//$data["role_id"]=$this->em->find('CsnUser\Entity\Role', $data["role_id"]["id"]);
    	//unset($data["display_name"]);
		error_log(serialize($data));
		if ($data["resource"]["id"]>0) {
			$data["resource"] = $this->em->getReference("CsnAuthorization\Entity\Resource", $data["resource"]["id"]);
		}else{
			unset($data["resource"]);
		}
		error_log(serialize($data));
      return parent::update($data);

	}
}