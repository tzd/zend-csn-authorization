<?php
 
namespace CsnAuthorization\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use CsnAuthorization\Entity\Privilege;
use Doctrine\ORM\EntityManager;
 
class ResourceController extends AbstractActionController
{
    protected $em;
 
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        }
        return $this->em;
    }
 
    public function indexAction()
    {
        return new ViewModel();
 
    }
 
    public function listAction()
    {
       //return new ViewModel();

        $listView = new ViewModel();
        $listView->setTemplate('layout/list');
        $gridColumns = new ViewModel();
        $gridColumns->setTemplate("resource/list/gridColumns");
        $buttonSearch = new ViewModel();
        $buttonSearch->setTemplate("resource/list/buttonSearch");
        $fieldSearch = new ViewModel();
        $fieldSearch->setTemplate("resource/list/fieldSearch");
        $listView->addChild($gridColumns, 'gridColumns')
             ->addChild($buttonSearch, 'buttonSearch')
             ->addChild($fieldSearch, 'fieldSearch'); 
        return $listView;
    }

    public function formAction()
    {
       //return new ViewModel();

        $listView = new ViewModel();
        $listView->setTemplate('layout/form');
        $formFields = new ViewModel();
        $formFields->setTemplate("resource/form/formFields");
        $listView->addChild($formFields, 'formFields');

        return $listView; 
    }
}