<?php


namespace CsnAuthorization\Controller;

use Zend\View\Model\JsonModel;
use CsnAuthorization\Entity\Privilege;
use Application\Controller\GumgaRestController;

class PrivilegeRestController extends GumgaRestController
{

    public function __construct()
    {
	 	parent::setNomeModulo("CsnAuthorization");
		parent::setNomeEntidade("Privilege");
    }
    
	public function getFields() {
		$fields = array();
		$fields['id'] = "getId";
		$fields['resource'] = "getResource";
		$fields['role'] = "getRole";
		$fields['permission_allow'] = "getPermissionAllow";
		$fields['name'] = "getName";
		$fields['label'] = "getLabel";
		$fields['parents'] = "getParents";
		return $fields;
	}

	public function getField($field,$record,$method = "")
	{
		if ($field == "role")
		{
			if (is_object($record->getRole()))
			return $record->getRole()->toArray();
		}else if ($field == "resource")
		{
			if (is_object($record->getResource()))
			return $record->getResource()->toArray();
		}else if ($field == "parents")
		{
			$parents = array();
			if (is_object($record->getParents())) {
				foreach ($record->getParents() as $key => $parent_privilege) {
error_log("parent_privilege");
error_log(serialize($parent_privilege->toArray()));
					$parents[]=$parent_privilege->toArray();
				}
			}
			return $parents;
		}else{
			return parent::getField($field,$record,$method);
		}
	}
}
