<?php


namespace CsnAuthorization\Controller;

use Zend\View\Model\JsonModel;
use CsnAuthorization\Entity\Resource;
use Application\Controller\GumgaRestController;

class ResourceRestController extends GumgaRestController
{

    public function __construct()
    {
	 	parent::setNomeModulo("CsnAuthorization");
		parent::setNomeEntidade("Resource");
    }
    
	public function getFields() {
		$fields = array();
		$fields['id'] = "getId";
		$fields['name'] = "getName";
		$fields['label'] = "getLabel";
		//$fields['resource'] = "getResource";
		return $fields;
	}

	public function getField($field,$record,$method = "")
	{
		if ($field == "role_id")
		{
			if (is_object($record->getRole()))
			return $record->getRole()->toArray();
		}else if ($field == "resource")
		{
			//if (is_object($record->getResource()))
			//return $record->getResource()->toArray();
		}else{
			return parent::getField($field,$record,$method);
		}
	}
}
