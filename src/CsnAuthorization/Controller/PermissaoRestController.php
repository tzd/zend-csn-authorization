<?php


namespace CsnAuthorization\Controller;

//use CsnAuthorization\Controller\PrivilegeRestController;
//class PermissaoRestController extends PrivilegeRestController

use Application\Controller\GumgaRestController;

class PermissaoRestController extends GumgaRestController
{

//     public function __construct()
//     {
// 	 	parent::__construct();
// 	 	$criterios = array();
// 	 	$i=0;
// 	 	$criterios[$i]["tipo"]="and";
// 	 	$criterios[$i]["campo"]="label";
// 	 	$criterios[$i]["valor"]="";
// 	 	$criterios[$i]["condicao"]="diferente";
// 	 	// $i++;
// 	 	// $criterios[$i]["tipo"]="and";
// 	 	// $criterios[$i]["campo"]="label";
// 	 	// $criterios[$i]["valor"]="";
// 	 	// $criterios[$i]["condicao"]="diferente";

// //	 	$auth = $this->identity();
// //	 	error_log("auth");
// //	 	error_log($auth);
// //		error_log(serialize($criterios));
// 	 	parent::setCriterioPadrao($criterios);
//     }


    public function __construct()
    {
	 	parent::setNomeModulo("CsnUser");
		parent::setNomeEntidade("Role");
    }
	public function getFields() {
		$fields = array();
		$fields['id'] = "getId";
		$fields['name'] = "getName";
		$fields['parents'] = "getParents";
		$fields['privileges'] = "getPrivileges";
		return $fields;
	}


	public function getField($field,$record,$method = "")
	{
//error_log("fields == $field");
		if ($field == "role")
		{
			if (is_object($record->getRole()))
			return $record->getRole()->toArray();
		}else if ($field == "resource")
		{
			if (is_object($record->getResource()))
			return $record->getResource()->toArray();
		}else if ($field == "parents")
		{
			$parents = array();
			if (is_object($record->getParents())) {
				foreach ($record->getParents() as $key => $parent_role) {
					error_log("passou $key");
					$parents[]=$parent_role->toArray();
				}
			}
			return $parents;
		}else if ($field == "privileges")
		{
			$privileges = array();
//error_log("parent_privilege");
			if (is_object($record->getPrivileges())) {
				foreach ($record->getPrivileges() as $key => $privilege) {
//error_log("parent_privilege");
//error_log(serialize($privilege->toArray()));
					$privileges[]=$privilege->toArray();
				}
			}
			return $privileges;
		}else{
			return parent::getField($field,$record,$method);
		}
	}
}
