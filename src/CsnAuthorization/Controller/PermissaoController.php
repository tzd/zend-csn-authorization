<?php
 
namespace CsnAuthorization\Controller;
 
use CsnAuthorization\Controller\PrivilegeController;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use CsnAuthorization\Entity\Privilege;
use Doctrine\ORM\EntityManager;
class PermissaoController extends PrivilegeController
{
    public function listAction()
    {
       //return new ViewModel();

        $listView = new ViewModel();
        $listView->setTemplate('layout/list');
        $gridColumns = new ViewModel();
        $gridColumns->setTemplate("permissao/list/gridColumns");
        $buttonSearch = new ViewModel();
        $buttonSearch->setTemplate("permissao/list/buttonSearch");
        $fieldSearch = new ViewModel();
        $fieldSearch->setTemplate("permissao/list/fieldSearch");
        $listView->addChild($gridColumns, 'gridColumns')
             ->addChild($buttonSearch, 'buttonSearch')
             ->addChild($fieldSearch, 'fieldSearch'); 
        return $listView;
    }

    public function formAction()
    {
       //return new ViewModel();

        $listView = new ViewModel();
        $listView->setTemplate('layout/form');
        $formFields = new ViewModel();
        $formFields->setTemplate("permissao/form/formFields");
        $listView->addChild($formFields, 'formFields');

        return $listView; 
    }
}