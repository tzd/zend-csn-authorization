<?php
/**
 * Coolcsn Zend Framework 2 Authorization Module
 * 
 * @link https://github.com/coolcsn/CsnAuthorization for the canonical source repository
 * @copyright Copyright (c) 2005-2013 LightSoft 2005 Ltd. Bulgaria
 * @license https://github.com/coolcsn/CsnAuthorization/blob/master/LICENSE BSDLicense
 * @author Stoyan Cheresharov <stoyan@coolcsn.com>, Stoyan Revov <st.revov@gmail.com>
*/

namespace CsnAuthorization;

return array(
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity',
                ),
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                )
            )
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'acl' => function ($sm) {
                $config = $sm->get('config');
                if ($config['acl']['use_database_storage'])
                    return new \CsnAuthorization\Acl\AclDb($sm->get('doctrine.entitymanager.orm_default'));
                else
                    return new \CsnAuthorization\Acl\Acl($config);
            }
        ),
    ),
    'view_helpers' => array(
        'factories' => array(
            'isAllowed' => function($sm) {
              $sm = $sm->getServiceLocator(); // $sm was the view helper's locator
              $auth = $sm->get('Zend\Authentication\AuthenticationService');
              $acl = $sm->get('acl');

              $helper = new \CsnAuthorization\View\Helper\IsAllowed($auth, $acl);
              return $helper;
            }
        ),
    ),
    'controller_plugins' => array(
        'factories' => array(
            'isAllowed' => function($sm) {
              $sm = $sm->getServiceLocator(); // $sm was the view helper's locator
              $auth = $sm->get('Zend\Authentication\AuthenticationService');
              $acl = $sm->get('acl');

              $plugin = new \CsnAuthorization\Controller\Plugin\IsAllowed($auth, $acl);
              return $plugin;
            }
        ),
    ),
    'router' => array (
        'routes' => array(
            'resource-admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/resource[/:action][/:id][/:state]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'state' => '[0-9]',
                    ),
                    'defaults' => array(
                        'controller' => 'CsnAuthorization\Controller\Resource',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
            'permissao-admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/permissao[/:action][/:id][/:state]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'state' => '[0-9]',
                    ),
                    'defaults' => array(
                        'controller' => 'CsnAuthorization\Controller\Permissao',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
            'privilege-admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/privilege[/:action][/:id][/:state]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'state' => '[0-9]',
                    ),
                    'defaults' => array(
                        'controller' => 'CsnAuthorization\Controller\Privilege',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
             'privilegerest' => array(
                 'type'    => 'literal',
                 'options' => array(
                     'route'    => '/api/privilege',
                     'defaults' => array(
                         'controller' => 'CsnAuthorization\Controller\PrivilegeRest',
                     ),
                 ),
                 'may_terminate' => true,
                 'child_routes' => array(
                    'new' => array(
                         'verb' => 'post',
                         'type'    => 'literal',
                         'options' => array(
                             'route'    => '/new',
                             'defaults' => array(
                                'controller' => 'CsnAuthorization\Controller\PrivilegeRest',
                                'action' => 'novo'
                             ),
                         ),
                    ),
                    'rest' => array(
                         'type'    => 'segment',
                         'options' => array(
                             'route'    => '[/:id]',
                             'constraints' => array(
                                 'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                 'id'     => '[0-9]+',
                             ),
                             'defaults' => array(
                               'controller' => 'CsnAuthorization\Controller\PrivilegeRest',
                                //'action' => 'teste'
                             ),
                         ),
                    ),
                 ),
             ),
             'resourcerest' => array(
                 'type'    => 'literal',
                 'options' => array(
                     'route'    => '/api/resource',
                     'defaults' => array(
                         'controller' => 'CsnAuthorization\Controller\ResourceRest',
                     ),
                 ),
                 'may_terminate' => true,
                 'child_routes' => array(
                    'new' => array(
                         'type'    => 'literal',
                         'options' => array(
                             'route'    => '/new',
                             'defaults' => array(
                                'controller' => 'CsnAuthorization\Controller\ResourceRest',
                                'action' => 'novo'
                             ),
                         ),
                    ),
                    'rest' => array(
                         'type'    => 'segment',
                         'options' => array(
                             'route'    => '[/:id]',
                             'constraints' => array(
                                 'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                 'id'     => '[0-9]+',
                             ),
                             'defaults' => array(
                               'controller' => 'CsnAuthorization\Controller\ResourceRest',
                                //'action' => 'teste'
                             ),
                         ),
                    ),
                 ),
             ),
             'permissaorest' => array(
                 'type'    => 'literal',
                 'options' => array(
                     'route'    => '/api/permissao',
                     'defaults' => array(
                         'controller' => 'CsnAuthorization\Controller\PermissaoRest',
                     ),
                 ),
                 'may_terminate' => true,
                 'child_routes' => array(
                    'new' => array(
                         'type'    => 'literal',
                         'options' => array(
                             'route'    => '/new',
                             'defaults' => array(
                                'controller' => 'CsnAuthorization\Controller\PermissaoRest',
                                'action' => 'novo'
                             ),
                         ),
                    ),
                    'rest' => array(
                         'type'    => 'segment',
                         'options' => array(
                             'route'    => '[/:id]',
                             'constraints' => array(
                                 'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                 'id'     => '[0-9]+',
                             ),
                             'defaults' => array(
                               'controller' => 'CsnAuthorization\Controller\PermissaoRest',
                                //'action' => 'teste'
                             ),
                         ),
                    ),
                 ),
             ),
        )
    ),
    'view_manager' => array(
        'display_exceptions' => true,
        'template_path_stack' => array(
            'csn-authorization' => __DIR__ . '/../view',
         ),
        'template_map' => array(
            'privilege/list/buttonSearch' => __DIR__ . '/../view/layout/privilege/list/buttonSearch.phtml',
            'privilege/list/fieldSearch' => __DIR__ . '/../view/layout/privilege/list/fieldSearch.phtml',
            'privilege/list/gridColumns' => __DIR__ . '/../view/layout/privilege/list/gridColumns.phtml',
            'privilege/form/formFields' => __DIR__ . '/../view/layout/privilege/form/fields.phtml',
            'permissao/list/buttonSearch' => __DIR__ . '/../view/layout/permissao/list/buttonSearch.phtml',
            'permissao/list/fieldSearch' => __DIR__ . '/../view/layout/permissao/list/fieldSearch.phtml',
            'permissao/list/gridColumns' => __DIR__ . '/../view/layout/permissao/list/gridColumns.phtml',
            'permissao/form/formFields' => __DIR__ . '/../view/layout/permissao/form/fields.phtml',
            'resource/list/buttonSearch' => __DIR__ . '/../view/layout/resource/list/buttonSearch.phtml',
            'resource/list/fieldSearch' => __DIR__ . '/../view/layout/resource/list/fieldSearch.phtml',
            'resource/list/gridColumns' => __DIR__ . '/../view/layout/resource/list/gridColumns.phtml',
            'resource/form/formFields' => __DIR__ . '/../view/layout/resource/form/fields.phtml',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),        
    ),
    'controllers' => array(
        'invokables' => array(
            'CsnAuthorization\Controller\Resource' => 'CsnAuthorization\Controller\ResourceController',
            'CsnAuthorization\Controller\ResourceRest' => 'CsnAuthorization\Controller\ResourceRestController',
            'CsnAuthorization\Controller\Privilege' => 'CsnAuthorization\Controller\PrivilegeController',
            'CsnAuthorization\Controller\PrivilegeRest' => 'CsnAuthorization\Controller\PrivilegeRestController',
            'CsnAuthorization\Controller\Permissao' => 'CsnAuthorization\Controller\PermissaoController',
            'CsnAuthorization\Controller\PermissaoRest' => 'CsnAuthorization\Controller\PermissaoRestController',
        ),
    ),

);